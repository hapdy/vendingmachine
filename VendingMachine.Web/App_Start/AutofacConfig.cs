﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using VendingMachine.Services;
using VendingMachine.Web.Controllers;

namespace VendingMachine.Web
{
  public class AutofacConfig
  {
    public static void ConfigureContainer()
    {
      var builder = new ContainerBuilder();

      // Register dependencies in controllers
      builder.RegisterControllers(typeof(HomeController).Assembly);

      // Register dependencies in filter attributes
      builder.RegisterFilterProvider();

      // Register dependencies in custom views
      builder.RegisterSource(new ViewRegistrationSource());

      // Register our Data dependencies
      builder.RegisterAssemblyModules(typeof(VendingMachineService).Assembly);

      var container = builder.Build();

      // Set MVC DI resolver to use our Autofac container
      DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
    }
  }
}