﻿using System;
using System.Web.Mvc;
using VendingMachine.Models;
using VendingMachine.Services;

namespace VendingMachine.Web.Controllers
{
  public class HomeController : Controller
  {
    private readonly IVendingMachineService _serivce;

    public HomeController(IVendingMachineService serivce)
    {
      _serivce = serivce;
    }

    public ActionResult Index()
    {
      ViewBag.Title = "Vending Machine";

      return View("Index", _serivce.GetInfo());
    }

    public ActionResult BuyCash(string flavour)
    {
      try
      {
        _serivce.SellCanCash(flavour);
        return RedirectToAction("Index");
      }
      catch (Exception e)
      {
        ModelState.AddModelError("", e.Message);
        return Index();
      }
    }

    public ActionResult BuyCreditCard(string flavour)
    {
      try
      {
        _serivce.SellCanCreditCard(flavour);
        return RedirectToAction("Index");
      }
      catch (Exception e)
      {
        ModelState.AddModelError("", e.Message);
        return Index();
      }
    }

    public ActionResult Restock()
    {
      _serivce.Restock(new RestockViewModel
                       {
                         Cans = new []
                                {
                                  new RestockCan{Flavour = "Coke", Quantity = 10}, 
                                  new RestockCan{Flavour = "Fanta", Quantity = 10}, 
                                  new RestockCan{Flavour = "Sprite", Quantity = 10}, 
                                  new RestockCan{Flavour = "Lift", Quantity = 10}, 
                                  new RestockCan{Flavour = "Ginger Beer", Quantity = 10}, 
                                  new RestockCan{Flavour = "Water", Quantity = 10}
                                }
                       });

      return RedirectToAction("Index");
    }
  }
}
