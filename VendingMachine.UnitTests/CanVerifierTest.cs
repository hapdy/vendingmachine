﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models;
using VendingMachine.Services;
using VendingMachine.Services.Verifier;

namespace VendingMachine.UnitTests
{
  [TestClass]
  public class CanVerifierTest
  {
    [TestMethod]
    public void TestMethod1()
    {
      var verifier = new CanVerifier();

      Can can = null;

      Assert.IsFalse(ErrorThrown<NoCansAvailableException>(c => verifier.Verify(c), can));

      can = new Can
            {
              Flavour = "Test",
              QuantityAvailable = 0,
              Price = 1.50M
            };

      Assert.IsTrue(ErrorThrown<NoCansAvailableException>(c => verifier.Verify(c), can));


      can.QuantityAvailable += 1;

      Assert.IsFalse(ErrorThrown<NoCansAvailableException>(c => verifier.Verify(c), can));

    }

    private static bool ErrorThrown<T>(Func<Can, bool> run, Can model) where T : Exception
    {
      try
      {
        run.Invoke(model);
      }
      catch (T)
      {
        return true;
      }
      catch
      {
        return false;
      }
      return false;
    }
  }
}
