﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VendingMachine.Core;
using VendingMachine.Models;
using VendingMachine.Services;

namespace VendingMachine.UnitTests
{
  [TestClass]
  public class CanRepositoryTests
  {
    [TestMethod]
    public void RestockCansTest()
    {
      var restockItem1 = new RestockCan
                         {
                           Flavour = "Flavour 1",
                           Quantity = 20
                         };

      var restockItem2 = new RestockCan
                         {
                           Flavour = "Flavour 2",
                           Quantity = 30
                         };

      var canFactoryMock = new Mock<ICanFactory>();

      canFactoryMock.Setup(f => f.CreateCans(It.IsAny<string>(), It.IsAny<uint>()))
                    .Returns(new Can
                             {
                               Flavour = restockItem1.Flavour,
                               QuantityAvailable = restockItem1.Quantity,
                               Price = 1.50M
                             });

      var repo = new CanRepository(canFactoryMock.Object);



      repo.RestockCans(restockItem1.Flavour, restockItem1.Quantity);

      Assert.AreEqual(restockItem1.Quantity, repo.TotalCans);
      Assert.AreEqual(0, repo.CansSold);

      canFactoryMock.Setup(f => f.CreateCans(It.IsAny<string>(), It.IsAny<uint>()))
                    .Returns(new Can
                             {
                               Flavour = restockItem2.Flavour,
                               QuantityAvailable = restockItem2.Quantity,
                               Price = 1.50M
                             });

      repo.RestockCans(restockItem2.Flavour, restockItem2.Quantity);

      Assert.AreEqual(restockItem1.Quantity + restockItem2.Quantity, repo.TotalCans);
      Assert.AreEqual(0, repo.CansSold);
    }

    [TestMethod]
    public void TakeCanTest()
    {
      var restockItem1 = new RestockCan
                         {
                           Flavour = "Flavour 1",
                           Quantity = 20
                         };

      var canFactoryMock = new Mock<ICanFactory>();

      canFactoryMock.Setup(f => f.CreateCans(It.IsAny<string>(), It.IsAny<uint>()))
                    .Returns(new Can
                             {
                               Flavour = restockItem1.Flavour,
                               QuantityAvailable = restockItem1.Quantity,
                               Price = 1.50M
                             });

      var repo = new CanRepository(canFactoryMock.Object);



      repo.RestockCans(restockItem1.Flavour, restockItem1.Quantity);

      repo.TakeCan(restockItem1.Flavour);

      var can = repo.GetCan(restockItem1.Flavour);

      Assert.AreEqual(restockItem1.Quantity - 1, can.QuantityAvailable);
    }
  }
}
