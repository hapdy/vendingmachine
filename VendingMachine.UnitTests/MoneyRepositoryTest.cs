﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Services;

namespace VendingMachine.UnitTests
{
  [TestClass]
  public class MoneyRepositoryTest
  {
    [TestMethod]
    public void TakePaymentTest()
    {
      var money = new MoneyRepository();

      money.TakeCashPayment(50);

      Assert.AreEqual(50, money.GetCashHeld());
      Assert.AreEqual(0, money.GetCreditCardsTotal());

      money.TakeCreditCardPayment(100);

      Assert.AreEqual(50, money.GetCashHeld());
      Assert.AreEqual(100, money.GetCreditCardsTotal());

      money.Clear();

      Assert.AreEqual(0, money.GetCashHeld());
      Assert.AreEqual(0, money.GetCreditCardsTotal());

    }
  }
}
