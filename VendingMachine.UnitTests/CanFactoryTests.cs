﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Services;
using Moq;
using VendingMachine.Core;

namespace VendingMachine.UnitTests
{
  [TestClass]
  public class CanFactoryTests
  {
    [TestMethod]
    public void CreateCans()
    {
      var testValues = new
                       {
                         Flavour = "Test",
                         Quantity = (uint)25,
                         Price = 1.5M
                       };

      var configMock = new Mock<IVendingMachineConfig>();

      configMock.SetupGet(c => c.CanPrice).Returns(testValues.Price);

      var factory = new CanFactory(configMock.Object);

      var can = factory.CreateCans(testValues.Flavour, testValues.Quantity);

      Assert.IsNotNull(can);
      Assert.AreEqual(testValues.Flavour, can.Flavour);
      Assert.AreEqual(testValues.Quantity, can.QuantityAvailable);
      Assert.AreEqual(testValues.Price, can.Price);
    }
  }
}
