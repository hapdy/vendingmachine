﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models;
using VendingMachine.Services;
using VendingMachine.Services.Verifier;

namespace VendingMachine.UnitTests
{
  [TestClass]
  public class VendingMachineServiceTest
  {
    private static decimal _canPrice;

    [TestMethod]
    public void TestMethod1()
    {
      var testItem1 = new RestockCan
                      {
                        Flavour = "Test 1",
                        Quantity = 5
                      };
      var testItem2 = new RestockCan
                      {
                        Flavour = "Test 2",
                        Quantity = 10
                      };



      var restockModel = new RestockViewModel
                         {
                           Cans = new []
                                  {
                                    testItem1,
                                    testItem2
                                  }
                         };

      var service = GenerateVendingMachineService();

      service.Restock(restockModel);

      var expectedQty = testItem1.Quantity + testItem2.Quantity;
      var expectedCash = 0M;
      var expectedCC = 0M;

      var info = service.GetInfo();

      Assert.IsNotNull(info);
      Assert.AreEqual(expectedQty, info.TotalCans);
      Assert.AreEqual(expectedCash, info.CashHeld);
      Assert.AreEqual(expectedCC, info.CrediCardTotal);

      service.SellCanCash(testItem1.Flavour);
      info = service.GetInfo();
      expectedQty--;
      expectedCash += _canPrice;

      Assert.IsNotNull(info);
      Assert.AreEqual(expectedQty, info.TotalCans);
      Assert.AreEqual(expectedCash, info.CashHeld);
      Assert.AreEqual(expectedCC, info.CrediCardTotal);

      service.SellCanCreditCard(testItem1.Flavour);
      info = service.GetInfo();
      expectedQty--;
      expectedCC += _canPrice;

      Assert.IsNotNull(info);
      Assert.AreEqual(expectedQty, info.TotalCans);
      Assert.AreEqual(expectedCash, info.CashHeld);
      Assert.AreEqual(expectedCC, info.CrediCardTotal);

      service.Restock(restockModel);
      info = service.GetInfo();
      expectedQty+= testItem1.Quantity + testItem2.Quantity;
      expectedCash = 0M;
      expectedCC = 0M;

      Assert.IsNotNull(info);
      Assert.AreEqual(expectedQty, info.TotalCans);
      Assert.AreEqual(expectedCash, info.CashHeld);
      Assert.AreEqual(expectedCC, info.CrediCardTotal);
    }


    private static VendingMachineService GenerateVendingMachineService()
    {
      var config = VendingMachineConfig.GetConfig();
      _canPrice = config.CanPrice;

      var factory = new CanFactory(config);
      var cans = new CanRepository(factory);
      var money = new MoneyRepository();

      var verifier = new CanVerifier();

      var service = new VendingMachineService(cans,
                                              verifier,
                                              money);

      return service;
    }
  }
}
