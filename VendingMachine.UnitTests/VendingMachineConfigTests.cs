﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Services;

namespace VendingMachine.UnitTests
{
  [TestClass]
  public class VendingMachineConfigTests
  {
    [TestMethod]
    public void CanPriceTest()
    {
      var config = VendingMachineConfig.GetConfig();

      Assert.IsNotNull(config);
      Assert.AreEqual(1.5M, config.CanPrice);
    }
  }
}
