﻿using System.Configuration;

namespace VendingMachine.Services
{
  public class VendingMachineConfig : ConfigurationSection, IVendingMachineConfig
  {

    [ConfigurationProperty("canPrice", DefaultValue = "0", IsRequired = true)]
    public decimal CanPrice
    {
      get => (decimal)this["canPrice"];
      set => this["canPrice"] = value;
    }

    public static VendingMachineConfig GetConfig()
    {
      return ConfigurationManager.GetSection("vendingMachine") as VendingMachineConfig;
    }
  }
}