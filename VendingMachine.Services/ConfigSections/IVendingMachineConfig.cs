﻿namespace VendingMachine.Services
{
  /// <summary>
  /// Gets configuation values for the vendig machine service
  /// </summary>
  public interface IVendingMachineConfig
  {
    /// <summary>
    /// The prive of the cans
    /// </summary>
    decimal CanPrice { get; }
  }
}