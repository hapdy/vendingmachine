﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Models;
using VendingMachine.Services.Verifier;

namespace VendingMachine.Services
{
  public class VendingMachineService : IVendingMachineService
  {
    private readonly ICanRepository _cans;

    private readonly ICanVerifier _canVerifier;
    private readonly IMoneyRepository _money;

    private readonly object _lock = new object();

    public VendingMachineService(ICanRepository cans, ICanVerifier canVerifier, IMoneyRepository money)
    {
      _cans = cans;

      _canVerifier = canVerifier;

      _money = money;
    }

    public VendingMachineInfo GetInfo()
    {
      lock (_lock)
      {
        return new VendingMachineInfo
               {
                 CashHeld = _money.GetCashHeld(),
                 CrediCardTotal = _money.GetCreditCardsTotal(),
                 TotalCans = _cans.TotalCans,
                 CansSold = _cans.CansSold,
                 Cans = _cans.GetAllCans()
               };
      }
    }
    public void Restock(RestockViewModel model)
    {
      lock (_lock)
      {
        _money.Clear();

        var cans = model?.Cans.ToList() ?? new List<RestockCan>();

        if (!cans.Any())
          return;

        cans.ForEach(c =>
                     {
                       _cans.RestockCans(c.Flavour, c.Quantity);
                     });
      }
    }

    public bool SellCanCash(string flavour)
    {
      lock (_lock)
      {
        var can = GetCanToSell(flavour);

        _money.TakeCashPayment(can.Price);
      }
      return true;
    }

    public bool SellCanCreditCard(string flavour)
    {
      lock (_lock)
      {
        var can = GetCanToSell(flavour);

        _money.TakeCreditCardPayment(can.Price);
      }
      return true;
    }

    private Can GetCanToSell(string flavour)
    {
      var can = _cans.GetCan(flavour);
      _canVerifier.Verify(can);

      _cans.TakeCan(flavour);

      return can;
    }

  }
}