﻿namespace VendingMachine.Services
{

  /// <summary>
  /// Repository for money
  /// </summary>
  public interface IMoneyRepository
  {
    /// <summary>
    /// Total cash held
    /// </summary>
    /// <returns></returns>
    decimal GetCashHeld();

    /// <summary>
    /// The total of credit card payments keep
    /// </summary>
    /// <returns></returns>
    decimal GetCreditCardsTotal();

    /// <summary>
    /// Takes a payment of cash and adds it to the repository
    /// </summary>
    /// <param name="amount">The amount of cash to add</param>
    void TakeCashPayment(decimal amount);

    /// <summary>
    /// Takes a credit card payment and adds it to the repository
    /// </summary>
    /// <param name="amount">The amount to add</param>
    void TakeCreditCardPayment(decimal amount);

    void Clear();
  }
}