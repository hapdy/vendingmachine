﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Models;

namespace VendingMachine.Services
{
  public class CanRepository : ICanRepository
  {
    private readonly ICanFactory _canFactory;

    private readonly IDictionary<string, Can> _cans;

    public CanRepository(ICanFactory canFactory)
    {
      _canFactory = canFactory;

      _cans = new Dictionary<string, Can>();
    }

    public void RestockCans(string flavour, uint quantity)
    {
      CansSold = 0;
      if (_cans.ContainsKey(flavour))
      {
        _cans[flavour].QuantityAvailable += quantity;
        return;
      }
      _cans.Add(flavour, _canFactory.CreateCans(flavour, quantity));
    }

    public Can GetCan(string flavour)
    {
      if (!_cans.ContainsKey(flavour))
      {
        throw new UnknownFlavourException(flavour);
      }

      return _cans[flavour];
    }

    public void TakeCan(string flavour)
    {
      if (!_cans.ContainsKey(flavour))
      {
        throw new UnknownFlavourException(flavour);
      }

      CansSold++;
      _cans[flavour].QuantityAvailable--;
    }

    public long TotalCans
    {
      get
      {
        return _cans?.Values
                    .Sum(c => c.QuantityAvailable) ?? 0L;
      }
    }

    public long CansSold { get; private set; }

    public IEnumerable<Can> GetAllCans()
    {
      return _cans?.Values ?? new Can[] { };
    }

  }
}