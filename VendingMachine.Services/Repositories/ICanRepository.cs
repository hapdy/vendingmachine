﻿using System.Collections.Generic;
using VendingMachine.Models;

namespace VendingMachine.Services
{
  /// <summary>
  /// Repository for can objects
  /// </summary>
  public interface ICanRepository
  {
    /// <summary>
    /// Restocks a can flavour
    /// </summary>
    /// <param name="flavour">The flavour of the can to resock</param>
    /// <param name="quantity">The number of cans to of a particular flavour to restock</param>
    void RestockCans(string flavour, uint quantity);

    /// <summary>
    /// Gets a can object by it's flavour
    /// </summary>
    /// <param name="flavour"></param>
    /// <returns></returns>
    Can GetCan(string flavour);

    /// <summary>
    /// Takes a can from a flavour
    /// </summary>
    /// <param name="flavour"></param>
    void TakeCan(string flavour);

    /// <summary>
    /// Returns the total number of can in a repository
    /// </summary>
    /// <returns></returns>
    long TotalCans { get; }

    /// <summary>
    /// Count of cans sold
    /// </summary>
    long CansSold { get; }

    /// <summary>
    /// Gets a list of all cans
    /// </summary>
    /// <returns></returns>
    IEnumerable<Can> GetAllCans();
  }
}