﻿namespace VendingMachine.Services
{
  public class MoneyRepository : IMoneyRepository
  {

    private decimal _cashHeld;
    private decimal _creditCardTotal;

    public decimal GetCashHeld()
    {
      return _cashHeld;
    }

    public decimal GetCreditCardsTotal()
    {
      return _creditCardTotal;
    }

    public void TakeCashPayment(decimal amount)
    {
      _cashHeld += amount;
    }

    public void TakeCreditCardPayment(decimal amount)
    {
      _creditCardTotal += amount;
    }

    public void Clear()
    {
      _cashHeld = 0;
      _creditCardTotal = 0;
    }
  }
}