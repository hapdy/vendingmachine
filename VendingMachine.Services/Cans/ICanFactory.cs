﻿using VendingMachine.Models;

namespace VendingMachine.Services
{
  /// <summary>
  /// A factory to create cans
  /// </summary>
  public interface ICanFactory
  {
    /// <summary>
    /// Creates a collection of Can objects
    /// </summary>
    /// <param name="flavour">The flavour of cans to create</param>
    /// <param name="quantity">The number of cans to create</param>
    /// <returns>A collection of Can objects</returns>
    Can CreateCans(string flavour, uint quantity);
  }
}