﻿using Autofac;

namespace VendingMachine.Services
{
  public class CanModule : Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      builder.RegisterType<CanFactory>()
        .AsImplementedInterfaces()
        .SingleInstance();

      base.Load(builder);
    }
  }
}