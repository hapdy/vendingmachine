﻿using System;
using VendingMachine.Models;

namespace VendingMachine.Services
{
  /// <summary>
  /// Creates a new can object
  /// </summary>
  public class CanFactory : ICanFactory
  {
    private readonly IVendingMachineConfig _vendingMachineConfig;

    public CanFactory(IVendingMachineConfig vendingMachineConfig)
    {
      _vendingMachineConfig = vendingMachineConfig;

      if (_vendingMachineConfig.CanPrice <= 0)
        throw new ArgumentException("Can price must be more than 0");
    }

    /// <summary>
    /// Creates a collection of Can objects
    /// </summary>
    /// <param name="flavour">The flavour of cans to create</param>
    /// <param name="quantity">The number of cans to create</param>
    /// <returns>A collection of Can objects</returns>
    public Can CreateCans(string flavour, uint quantity)
    {
      if (string.IsNullOrWhiteSpace(flavour))
        throw new ArgumentNullException(nameof(flavour));

      if (quantity == 0)
        throw new ArgumentException($"{nameof(quantity)} must be greater than 0", nameof(quantity));

      return new Can
      {
        Flavour = flavour,
        Price = _vendingMachineConfig.CanPrice,
        QuantityAvailable = quantity
      };
    }
  }
}