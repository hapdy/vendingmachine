﻿using System;

namespace VendingMachine.Services
{
  public class UnknownFlavourException : Exception
  {
    public string Flavour { get; }


    public UnknownFlavourException(string flavour) : base($"{flavour ?? "This flavour"} is not available")
    {
      Flavour = flavour;
    }
  }
}