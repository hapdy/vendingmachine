﻿using System;

namespace VendingMachine.Services
{
  public class NoCansAvailableException : Exception
  {
    public string Flavour { get; }

    public NoCansAvailableException(string flavour) : base($"We are out of cans of {flavour ?? "this flavour"}")
    {
      Flavour = flavour;
    }
  }
}