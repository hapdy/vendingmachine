﻿using VendingMachine.Models;

namespace VendingMachine.Services
{
  /// <summary>
  /// A service to model the behaviour of a vending machine
  /// </summary>
  public interface IVendingMachineService
  {
    /// <summary>
    /// Gets the current information about a vending machine
    /// </summary>
    /// <returns></returns>
    VendingMachineInfo GetInfo();

    /// <summary>
    /// Restocks the vending machine and clears the payments
    /// </summary>
    /// <param name="model">The infomation model used to restock the vending machine</param>
    void Restock(RestockViewModel model);

    /// <summary>
    /// Sells a can of a particual flavour using cash
    /// </summary>
    /// <param name="flavour">The flavour of the can to sell</param>
    /// <returns></returns>
    bool SellCanCash(string flavour);

    /// <summary>
    /// Sells a can of a particual flavour using credit card
    /// </summary>
    /// <param name="flavour">The flavour of the can to sell</param>
    /// <returns></returns>
    bool SellCanCreditCard(string flavour);
  }
}