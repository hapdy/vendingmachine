﻿using VendingMachine.Core;
using VendingMachine.Models;

namespace VendingMachine.Services.Verifier
{
  public interface ICanVerifier: IVerifier<Can>
  {
  }
}