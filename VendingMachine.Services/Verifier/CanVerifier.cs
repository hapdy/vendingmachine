﻿using VendingMachine.Models;

namespace VendingMachine.Services.Verifier
{
  public class CanVerifier : ICanVerifier
  {
    public bool Verify(Can can)
    {
      if (can.QuantityAvailable == 0)
      {
        throw new NoCansAvailableException(can.Flavour);
      }

      return true;
    }
  }
}