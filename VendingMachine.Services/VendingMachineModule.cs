﻿using Autofac;
using VendingMachine.Services.Verifier;

namespace VendingMachine.Services
{
  public class VendingMachineModule : Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      builder.RegisterInstance(VendingMachineConfig.GetConfig())
             .AsImplementedInterfaces()
             .SingleInstance();

      builder.RegisterType<CanRepository>()
             .AsImplementedInterfaces()
             .SingleInstance();

      builder.RegisterType<CanVerifier>()
             .AsImplementedInterfaces()
             .InstancePerDependency();

      builder.RegisterType<MoneyRepository>()
             .AsImplementedInterfaces()
             .InstancePerDependency();

      builder.RegisterType<VendingMachineService>()
             .AsSelf()
             .AsImplementedInterfaces()
             .SingleInstance();

      base.Load(builder);
    }
  }
}