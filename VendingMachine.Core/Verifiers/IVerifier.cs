﻿namespace VendingMachine.Core
{
  public interface IVerifier<in T>
  {
    bool Verify(T model);
  }
}