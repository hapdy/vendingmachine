﻿using System.Collections.Generic;

namespace VendingMachine.Models
{
  public class RestockViewModel
  {
    public IEnumerable<RestockCan> Cans {get;set;}
  }
}