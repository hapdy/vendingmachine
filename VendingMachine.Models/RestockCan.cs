﻿namespace VendingMachine.Models
{
  public class RestockCan
  {
    public string Flavour { get; set; }
    public uint Quantity { get; set; }
  }
}