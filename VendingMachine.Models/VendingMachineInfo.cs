﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VendingMachine.Models
{
  public class VendingMachineInfo
  {
    [DisplayFormat(DataFormatString = "{0:C}")]
    public decimal CashHeld { get; set; }

    [DisplayFormat(DataFormatString = "{0:C}")]
    public decimal CrediCardTotal { get; set; }

    public long CansSold { get; set; }

    public long TotalCans { get; set; }

    public IEnumerable<Can> Cans { get; set; }
  }
}