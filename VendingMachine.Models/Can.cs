﻿namespace VendingMachine.Models
{
  /// <summary>
  /// A can used in a vending machine
  /// </summary>
  public class Can
  {
    /// <summary>
    /// Flavour of the can
    /// </summary>
    public string Flavour { get; set; }

    /// <summary>
    /// The Price of the can
    /// </summary>
    /// <remarks>Price is fixed and the same for all flavours</remarks>
    public decimal Price { get; set; }

    /// <summary>
    /// Number of available cans for sale
    /// </summary>
    public uint QuantityAvailable { get; set; }

    public void Restock(uint quantityToAdd)
    {
      QuantityAvailable += quantityToAdd;
    }
  }
}
