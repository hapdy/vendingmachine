# Vending Machine: Technical Exam README #

## Task ##

Provide a software solution to model the operation of a vending machine. This can be done in the language and technology of your choice (console, desktop app, web). The elegance of the user interface is less important than the elegance of the underlying architecture. All requirements must be implemented. Any assumptions should be stated. 
The vending machine can stock up to 10 different flavours of cans, but they are currently all the same price. The value of cans may be decided by you at design time. 
The vending machine can take payment either through cash or credit card.

The vending machine can eject one can per payment.

The vending machine will need to track the number of cans available, the amount of money currently held in the machine, and the amount of credit card payments made. This information can be held in memory during runtime and does not necessarily need to be persisted. 
Restocking the machine assumes that

* the amount of cash held in the machine has been reset to zero
* the amount of credit card payments has been reset to zero
* the number of cans sold has been reset to zero
* the amount of cans added during the restock is added to the available cans

Assume that if paying with cash, correct change is always used. 

Looking for unit tests, DI especially around the payment methods, possibly around can types and whether they choose to implement concrete can types or something else. 

## Assumptions ##

1. Appart from flavour, cans are identical, and therefore no other information is required per can.
2. No further information about a cans flavour beyond it's name is required.
3. Can's are not kept in lanes but in a one pool and any can can be selected without being blocked by another
4. No maximum can limit was provided, so no limit will be enforced
5. No maximum limit of cans that can be added in a restock was provided, so no limit will be enforced

